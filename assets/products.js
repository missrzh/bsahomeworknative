export var db = [{
    title: "Polaroid Now+",
    price: "149$",
    sellerName: "Sasha",
    sellerPhone: "+38 096 134 87 56",
    sellerAvatar: "https://new-retail.ru/upload/iblock/993/x99303998cb738ffa64b7c8cd21638bfe.jpg.pagespeed.ic.Jx8YXPG0F5.webp",
    photo: ["https://cdn.shopify.com/s/files/1/1162/8964/products/009062_itype-now-plus_polaroid_camera_white_angle_left_828x.png", "https://cdn.shopify.com/s/files/1/1162/8964/products/009062_itype-now-plus_polaroid_camera_white_front_tilted_828x.png?v=1639126649", "https://cdn.shopify.com/s/files/1/1162/8964/products/009062_itype-now-plus_polaroid_camera_white_top_828x.png?v=1639126649"],
    description: "Our most creative camera yet. Polaroid Now+ is our revamped analog instant camera with even more creative tools. Get 5 new lens filters, and unlock two extra tools — aperture priority and tripod mode — inside the Polaroid mobile app. Try light painting, double exposure, manual mode and more. Plus a tripod mount to steady your ideas inside the original Polaroid square format frame.Unlock extra creative tools inside the Polaroid mobile app. Find focus easily with aperture priority and create cinematic photos with tripod mode. Get these two new tools, plus light painting, double exposure, manual mode, and more.",
}, {
    title: "Polaroid Now",
    price: "129$",
    sellerName: "Sasha",
    sellerPhone: "+38 096 134 87 56",
    sellerAvatar: "https://new-retail.ru/upload/iblock/993/x99303998cb738ffa64b7c8cd21638bfe.jpg.pagespeed.ic.Jx8YXPG0F5.webp",
    photo: ["https://cdn.shopify.com/s/files/1/1162/8964/products/now_black-polaroid-camera_009028_right-angle_a0d52225-9a53-4b34-ac35-fcbcc2690149_828x.png", "https://cdn.shopify.com/s/files/1/1162/8964/products/now_black-polaroid-camera_009028_front-tilted_76e1ecda-e4c5-48b9-87c1-01223d41e88c_828x.png?v=1639648962", "https://cdn.shopify.com/s/files/1/1162/8964/products/now_black-polaroid-camera_009028_top_0145e09a-d002-488c-beb1-bfc160125994_828x.png?v=1639648962", "https://cdn.shopify.com/s/files/1/1162/8964/products/now_black-polaroid-camera_009028_back_64337ba5-effb-482d-94ff-44fb3f6558b3_828x.png?v=1639648962"],
    description: "Capture and keep your everyday moments forever with the Polaroid Now. Our new analog instant camera comes with autofocus to help you catch life as you live it in that iconic Polaroid instant film format. In 7 colors, plus black and white, there’s a Polaroid Now to suit you.In other words: sharper photos, easily. The Polaroid Now decides which lens is suitable, so you get beautiful portraits in more places, more often — even without the flash.Get two frames in the one Polaroid photograph. Dreamy, artistic, and ready to add some drama to your photography.",
}, {
    title: "Polaroid Go",
    price: "119$",
    sellerName: "Nikita",
    sellerAvatar: "https://autogear.ru/misc/i/gallery/40072/1260203.jpg",
    sellerPhone: "+38 096 128 88 00",
    photo: ["https://cdn.shopify.com/s/files/1/1162/8964/products/image_go_polaroid_camera_009035_in_hand_2_828x.png?", "https://cdn.shopify.com/s/files/1/1162/8964/products/image_go_polaroid_camera_009035_front_tilted_828x.png?v=1634023765", "https://cdn.shopify.com/s/files/1/1162/8964/products/image_go_polaroid_camera_009035_top_828x.png?v=1634023765"],
    description: "Create big, smaller, with the new Polaroid Go camera. Meet your portable, wearable, partner in creativity: one small camera, lots of big ideas, and all the features you love from our classic models.Find your light with the reflective selfie mirror, and pick your pose with plenty of time, thanks to the self-timer.Add another layer to your creative vision with easy-to-use double exposure."
},
{
    title: "Polaroid 600",
    price: "129$",
    sellerName: "Polaroid",
    sellerPhone: "+38 096 122 00 00",
    sellerAvatar: "https://cdn.shopify.com/s/files/1/1162/8964/files/homepage_module_desktop_cameras_1136x.jpg?v=1640015756",
    photo: ["https://cdn.shopify.com/s/files/1/1162/8964/products/600-square-polaroid-camera_004708_angle-left_fb791d37-af0a-4f1f-b54a-53922ad261cd_828x.png", "https://cdn.shopify.com/s/files/1/1162/8964/products/600-square-polaroid-camera_004708_front_64574962-ec80-4702-ac2a-8b0fc220bd1f_828x.png", "https://cdn.shopify.com/s/files/1/1162/8964/products/600-square-polaroid-camera_004708_back_64be5c79-c314-4fc4-b813-5f30de701441_828x.png"],
    description: "Your first step into analog instant photography. The Polaroid 600 Square analog instant camera is as simple as point-and-shoot. The boxy, pop-up shape made it an icon when it was first released in 1981. A fully refurbished icon you can now own today. Pop culture in a camera. The point-and-shoot cameras that brought instant photography to the masses wrapped in the colors and characters that defined the ‘80s, ‘90s, and ‘00s. From boxy shapes to pop-up flashes, each camera tells a story that you’ll want to share. This is a fully refurbished vintage Polaroid camera. Each day we save original Polaroid cameras from decades past and lovingly restore them to give them a new life."
},
{
    title: "Color i‑Type Film ‑ Black Frame Edition",
    price: "17$",
    sellerName: "Polaroid",
    sellerPhone: "+38 096 122 00 00",
    sellerAvatar: "https://cdn.shopify.com/s/files/1/1162/8964/files/homepage_module_desktop_cameras_1136x.jpg?v=1640015756",
    photo: ["https://cdn.shopify.com/s/files/1/1162/8964/products/film_itype-color-film_black-frame_006019_front_polaroid_photo_828x.png", "https://cdn.shopify.com/s/files/1/1162/8964/products/film_itype-color-film_black-frame_006019_sample-4_828x.png?v=1604668510"],
    description: "A new generation of instant film made for our i-Type cameras. The original Polaroid format you love minus the battery, so it’s a little friendlier on the bank account too. Compatible with your Polaroid Now, Lab, or new OneStep camera.8 instant photographs in that original Polaroid square format and finished in a matte black frame.8 photos per pack, Black frames, Battery-free. Dress code: black. Our Black Frame Edition in color i-Type film makes the good times bold thanks to its vivid color chemistry and matte black frame. 8 party-ready photos in every pack."
},]