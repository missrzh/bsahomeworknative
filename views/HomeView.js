import React from 'react';
import { TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    Image,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';
import { db } from '../assets/products';



const Section = ({ item, navigation, children }) => {

    return (
        <TouchableOpacity
            accessible={true}
            onPress={() =>
                navigation.navigate('Product', { item: item })
            }>
            <View style={styles.sectionContainer}>
                <Image
                    style={styles.sectionTitle}
                    source={{
                        uri: item.photo[0],
                    }}
                />
                <Text numberOfLines={4}
                    style={styles.sectionDescription}>
                    <Text style={styles.highlight}>{item.title}{"\n"}</Text>
                    {item.description}
                </Text>
                <Text
                    style={styles.sectionPrice}>
                    {item.price}
                </Text>
            </View>
        </TouchableOpacity>
    );
};



export const HomeView = ({ navigation }) => {
    const backgroundStyle = {
        backgroundColor: Colors.white,
    };
    var products = []
    db.forEach((product, i) => {
        products.push(<Section item={product} navigation={navigation} key={i}></Section>)
    })
    return (
        <SafeAreaView style={backgroundStyle}>

            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={backgroundStyle}>
                <View>
                    {products}
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}
const styles = StyleSheet.create({
    sectionContainer: {
        justifyContent: "space-between",
        flexDirection: "row",
        marginTop: 32,
        paddingHorizontal: 10,
        alignContent: "space-between",
    },
    sectionTitle: {
        height: 100,
        flex: 0.5,
    },
    sectionDescription: {
        color: "black",
        paddingHorizontal: 10,
        flex: 1,
        fontSize: 18,
        fontWeight: '400',
    },
    sectionPrice: {
        color: "black",
        fontSize: 15,
        fontWeight: '400',
    },
    highlight: {
        fontWeight: '700',
    },
});