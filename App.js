import React from 'react';
import { HomeView } from './views/HomeView';
import { ItemView } from './views/ItemView';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeView}
          options={{ title: 'Shop' }}
        />
        <Stack.Screen name="Product" component={ItemView} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};


export default App;
