import React from 'react';
import { TouchableOpacity } from 'react-native';
import Gallery from 'react-native-image-gallery';
import {
    SafeAreaView,
    ScrollView,
    Image,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import call from 'react-native-phone-call'
import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';



export const ItemView = ({ route }) => {
    const backgroundStyle = {
        backgroundColor: Colors.white,
    };
    const args = {
        number: route.params.item.sellerPhone,
    }
    var listOfImg = []
    route.params.item.photo.forEach(img => {
        listOfImg.push({ source: { uri: img } })
    });
    return (
        <SafeAreaView style={backgroundStyle}>

            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={backgroundStyle}>
                <Gallery //взял готовую библиотеку, не разбирался до конца как она работает
                    style={{ height: 300 }}
                    images={listOfImg}
                />
                <View style={styles.maincont}>
                    <Text style={styles.title}>{route.params.item.title}</Text>
                    <Text style={styles.text}>{route.params.item.description}</Text>
                </View>
            </ScrollView>
            <View style={styles.stick}>
                <View style={styles.sellerInfo}>
                    <Image
                        style={styles.avatar}
                        source={{
                            uri: route.params.item.sellerAvatar,
                        }} />
                    <View>
                        <Text style={styles.name}>
                            {route.params.item.sellerName}
                        </Text>
                        <Text style={styles.phone}>
                            {route.params.item.sellerPhone}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={styles.callButton}
                    onPress={() => call(args).catch(console.error)}>
                    <Text style={styles.textButon}>
                        Call Seller

                    </Text>
                </TouchableOpacity>

            </View>

        </SafeAreaView>
    );
}
const styles = StyleSheet.create({
    container: {
        paddingTop: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    stretch: {
        width: 300,
        height: 300,

        flex: 1
    },
    text: {
        paddingHorizontal: 10,
        fontSize: 15,
        color: "black"
    },
    title: {
        paddingTop: 5,
        paddingLeft: 10,
        fontSize: 18,
        fontWeight: "bold",
        color: "black"
    },
    maincont: {
        paddingBottom: 140
    },

    stick: {
        padding: 10,

        position: 'absolute',
        height: 140,
        width: '100%',
        bottom: 0,
        backgroundColor: "white"
    },
    avatar: {
        width: 75,
        height: 75
    },
    name: {
        paddingLeft: 10,
        fontSize: 22,
        fontWeight: "bold",
        color: "black"
    },
    phone: {
        paddingLeft: 10,
        fontSize: 19,
        color: "black"
    },
    sellerInfo: {
        flexDirection: 'row',
    },
    callButton: {
        borderWidth: 2,
        borderColor: "black",
        marginTop: 10,
        backgroundColor: "white",
        width: 150,
        alignItems: "center"
    },
    textButon: {
        fontSize: 18,
        color: "black"
    }
});